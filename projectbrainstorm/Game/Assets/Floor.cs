﻿using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour {

    public GameObject player;
    private bool overrideChanging = false;

    Vector4 startC, destC;

    private Color col;
    private float h, s, v;

    public Material anyDiffMaterial;

    private float a = 0.8f;
    public float hueSpeed;
    private Shader oldShader;
    // Use this for initialization
    void Start () {
        col = GetComponent<Renderer>().material.color;

       RGBToHSV(col, out h, out s, out v);

    }
    public static void RGBToHSV(Color rgbColor, out float H, out float S, out float V)
    {
        if (rgbColor.b > rgbColor.g && rgbColor.b > rgbColor.r)
        {
            RGBToHSVHelper(4f, rgbColor.b, rgbColor.r, rgbColor.g, out H, out S, out V);
        }
        else
        {
            if (rgbColor.g > rgbColor.r)
            {
                RGBToHSVHelper(2f, rgbColor.g, rgbColor.b, rgbColor.r, out H, out S, out V);
            }
            else
            {
                RGBToHSVHelper(0f, rgbColor.r, rgbColor.g, rgbColor.b, out H, out S, out V);
            }
        }
    }

    private static void RGBToHSVHelper(float offset, float dominantcolor, float colorone, float colortwo, out float H, out float S, out float V)
    {
        V = dominantcolor;
        if (V != 0f)
        {
            float num = 0f;
            if (colorone > colortwo)
            {
                num = colortwo;
            }
            else
            {
                num = colorone;
            }
            float num2 = V - num;
            if (num2 != 0f)
            {
                S = num2 / V;
                H = offset + (colorone - colortwo) / num2;
            }
            else
            {
                S = 0f;
                H = offset + (colorone - colortwo);
            }
            H /= 6f;
            if (H < 0f)
            {
                H += 1f;
            }
        }
        else
        {
            S = 0f;
            H = 0f;
        }
    }

     public static Color HSVToRGB(float H, float S, float V)
 {
     if (S == 0f)
         return new Color(V,V,V);
     else if (V == 0f)
         return Color.black;
     else
     {
         Color col = Color.black;
         float Hval = H * 6f;
         int sel = Mathf.FloorToInt(Hval);
         float mod = Hval - sel;
         float v1 = V * (1f - S);
         float v2 = V * (1f - S * mod);
         float v3 = V * (1f - S * (1f - mod));
         switch (sel + 1)
         {
         case 0:
             col.r = V;
             col.g = v1;
             col.b = v2;
             break;
         case 1:
             col.r = V;
             col.g = v3;
             col.b = v1;
             break;
         case 2:
             col.r = v2;
             col.g = V;
             col.b = v1;
             break;
         case 3:
             col.r = v1;
             col.g = V;
             col.b = v3;
             break;
         case 4:
             col.r = v1;
             col.g = v2;
             col.b = V;
             break;
         case 5:
             col.r = v3;
             col.g = v1;
             col.b = V;
             break;
         case 6:
             col.r = V;
             col.g = v1;
             col.b = v2;
             break;
         case 7:
             col.r = V;
             col.g = v3;
             col.b = v1;
             break;
         }
         col.r = Mathf.Clamp(col.r, 0f, 1f);
         col.g = Mathf.Clamp(col.g, 0f, 1f);
         col.b = Mathf.Clamp(col.b, 0f, 1f);
         return col;
     }
 }
	
	// Update is called once per frame
	void Update () {
        Vector3 npo = player.transform.position;
        npo.y = 0;
        transform.position = npo;


        if (!overrideChanging)
        {
            h += Time.deltaTime * hueSpeed;
            if (h > 1)
            {
                h = 0;
            }
            Color mid = HSVToRGB(h, s, v);
            mid.a = a;
            GetComponent<Renderer>().material.color = mid;
        }
    }

    public IEnumerator ChangeForExplosion()
    {
        overrideChanging = true;
        startC = new Vector4(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.a);

        destC = new Vector4(0, 0.2f, 0.2f, 1);
        for (float f = 0; f < 1.0f; f += 3 * Time.deltaTime)
        {
            if (f > 1)
            {
                f = 1;
            }
            Vector4 newCol = Vector4.Lerp(startC, destC, f);

            GetComponent<Renderer>().material.color = new Color(newCol.x, newCol.y, newCol.z, newCol.w);
            yield return null;
        }

        oldShader = GetComponent<Renderer>().material.shader;
        GetComponent<Renderer>().material.shader = anyDiffMaterial.shader;

    }

    public IEnumerator ChangeBackAfterExplosion()
    {
        GetComponent<Renderer>().material.shader = oldShader;
        for (float f = 0; f < 1.0f; f += 3 * Time.deltaTime)
        {
            if (f > 1)
            {
                f = 1;
            }
            Vector4 newCol = Vector4.Lerp(destC, startC, f);

            GetComponent<Renderer>().material.color = new Color(newCol.x, newCol.y, newCol.z, newCol.w);
            yield return null;
        }
        overrideChanging = false;
    }
}
