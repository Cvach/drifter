﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Speedometer : MonoBehaviour {
	private GameObject player;

    private Color startColor;
    private Color slowColor = new Color(1, 1, 0, 1);
    private Color mediumColor = new Color(1, 185.0f / 255.0f, 50.0f / 255.0f, 1);
    private Color fastColor = new Color(240.0f / 255.0f, 150.0f / 255.0f, 0, 1);
    private Color superFastColor = new Color(1, 13.0f / 255.0f, 13.0f / 255.0f, 1);

    private float transitionSpeed = .3f;
	// Use this for initialization
	void Start () {
		player = transform.root.gameObject;
        startColor = gameObject.transform.GetComponent<Text>().color;
	}
	
	// Update is called once per frame
	void Update () {
		player = transform.root.gameObject;
		if(player.GetComponent<PlayerMovement>().getSecondsOfDelayLeft() <= 0)
		{
            //if (player.GetComponent<PlayerMovement>().playerSpeed > 0)
            //{
            //    transitionSpeed = .15f;
            //    gameObject.transform.GetComponent<Text>().color = Color.Lerp(startColor, slowColor, transitionSpeed);
            //}
            //if (player.GetComponent<PlayerMovement>().playerSpeed > 40)
            //{
            //    gameObject.transform.GetComponent<Text>().color = mediumColor;
            //}
            //if (player.GetComponent<PlayerMovement>().playerSpeed > 70)
            //{
            //    gameObject.transform.GetComponent<Text>().color = fastColor;
            //}
            //if (player.GetComponent<PlayerMovement>().playerSpeed > 100)
            //{
            //    gameObject.transform.GetComponent<Text>().color = superFastColor;
            //}
            gameObject.transform.GetComponent<Text>().color = Color.Lerp(startColor, fastColor, player.GetComponent<PlayerMovement>().playerSpeed/120.0f);
            transitionSpeed += Time.deltaTime;

            if (player.GetComponent<Rigidbody>().velocity.z > player.GetComponent<PlayerMovement>().bigMaxSpeed - 2)
            {
                gameObject.transform.GetComponent<Text>().text = "" + player.GetComponent<PlayerMovement>().bigMaxSpeed;
            }
            else if (player.GetComponent<Rigidbody>().velocity.z > player.GetComponent<PlayerMovement>().maxSpeed - 2 && player.GetComponent<Rigidbody>().velocity.z < player.GetComponent<PlayerMovement>().maxSpeed + 1)
            {
                gameObject.transform.GetComponent<Text>().text = "" + player.GetComponent<PlayerMovement>().maxSpeed;
            }
            else
            {
                gameObject.transform.GetComponent<Text>().text = Mathf.Floor(player.GetComponent<Rigidbody>().velocity.z).ToString();
            }
		}
		else
		{
			gameObject.transform.GetComponent<Text>().text = "0";
		}
		//transform.rotation = player.transform.FindChild("Bike").transform.rotation;
	}
}
