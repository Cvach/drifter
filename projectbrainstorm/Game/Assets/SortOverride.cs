﻿using UnityEngine;
using System.Collections;

public class SortOverride : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Camera>().transparencySortMode = TransparencySortMode.Orthographic;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
