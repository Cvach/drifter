﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JumpRingCombo : MonoBehaviour {

    public int minRings, maxRings;
    public float maxXChange, zChange, yChange;

    public static GameObject player;

    public List<GameObject> powerupsAtEnd;

    SkyFall sf;
    GameObject powerup = null;

    Vector3 GetRandomMod()
    {
        return new Vector3(Random.Range(-maxXChange, maxXChange), yChange, zChange);
    }

	// Use this for initialization
	void Start () {

        sf = transform.parent.gameObject.GetComponent<SkyFall>();
        if (maxRings > 0)
        {
            int r = maxRings == 0 ? minRings :  Random.Range(minRings, maxRings);
            JumpRingCombo newOne = ((JumpRingCombo)(Instantiate(this)));
            newOne.name = this.name;
            //newOne.transform.parent = transform.parent;
            newOne.transform.parent = transform.parent;
            newOne.transform.position = transform.position + GetRandomMod();
            newOne.minRings = r - 1;
            newOne.maxRings = r - 1;
        }

        else
        {
            //Instantiate a powerup at the position the next ring would work.
            int ran = Random.RandomRange(0, powerupsAtEnd.Count);
            powerup = (GameObject)Instantiate(powerupsAtEnd[ran]);
            powerup.transform.parent = transform.parent;
            powerup.transform.position = transform.position + GetRandomMod();
            
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if (sf.IsDone() && transform.parent != null)
        {
            transform.parent = null;
            if (powerup != null)
            {
                powerup.transform.parent = null;
            }


        }
        if (player.transform.position.z > transform.position.z)
        {
            Destroy(this);
        }
	}
}
