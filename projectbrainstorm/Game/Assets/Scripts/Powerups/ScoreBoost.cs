﻿using UnityEngine;
using System.Collections;

public class ScoreBoost : Ability {
    private static int hits = 0;
    public float baseScoreBoost = 10.0f;
    LerpToPlace lerper;

	// Use this for initialization
	void Start () {
        
	    lerper = this.gameObject.GetComponent<LerpToPlace>();
	}
	
	// Update is called once per frame
	void Update () {
        if (lerper != null)
        {

            if (lerper.isDone())
            {
                float toAdd = baseScoreBoost * ScoreManager.GetMultiplier();
                ScoreManager.AddToScore(toAdd);
                ScoreManager.flashScore();
                Destroy(this.gameObject);
            }
        }
	}

    public override void Activate()
    {
       // throw new System.NotImplementedException();
    }

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.Play("Bloop");
            }
            viewInfo(ref hits);
            //Debug.Log("Score Before: " + ScoreManager.GetCurrentScore());

            //Debug.Log("Score After: " + ScoreManager.GetCurrentScore());
            this.transform.parent = other.gameObject.transform;

            this.gameObject.GetComponent<Collider>().enabled = false; 
            if (lerper != null)
            {
                lerper.activate();
            }

            //Destroy(this.gameObject);
        }
    }

}
