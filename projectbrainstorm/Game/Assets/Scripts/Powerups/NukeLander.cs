﻿using UnityEngine;
using System.Collections;

public class NukeLander : Ability
{
    private static int hits = 0;
    public float blastRaduis = 300.0f;
    public float growthSpeed = 100.0f;
    public Mesh meshChanger;
    public Material newMaterial;
    // Use this for initialization
    void Start()
    {
        
    }

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.active)
        {
            if (blastRaduis > this.gameObject.GetComponent<Collider>().transform.localScale.x)
            {
                Vector3 toAdd = this.gameObject.GetComponent<Collider>().transform.localScale;
                toAdd.x += growthSpeed * Time.deltaTime;
                toAdd.y += growthSpeed * Time.deltaTime;

                toAdd.z += growthSpeed * Time.deltaTime;

                this.gameObject.GetComponent<Collider>().transform.localScale = toAdd;

            }
            else
            {

                Floor floor = FindObjectOfType<Floor>();

                floor.StartCoroutine("ChangeBackAfterExplosion");
                Destroy(this.gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            if (other.gameObject.transform.Find("NukeLander(Clone)"))
            {
                print("points for nuke");
                ScoreManager.AddToScore(10 * ScoreManager.GetMultiplier());
                ScoreManager.flashScore();
            }
            else
            {
                SoundManager.Instance.Play("Bloop");
                viewInfo(ref hits);
                this.gameObject.transform.parent = other.gameObject.transform;
                this.gameObject.transform.position = other.gameObject.transform.position;
                this.gameObject.GetComponent<MeshFilter>().mesh = meshChanger;
                this.gameObject.GetComponent<Renderer>().material = newMaterial;
            }

            //this.gameObject.renderer.enabled = false;
        }
        else if (other.gameObject.tag == "Obstacle")
        {
            ScoreManager.AddToScore(5);
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.name.Contains("Floor"))
        {
            Activate();
        }

    }



    public override void Activate()
    {
        this.active = true;
        SoundManager.Instance.Play("Bomb");
        
        Floor floor = FindObjectOfType<Floor>();

        floor.StartCoroutine("ChangeForExplosion");


    }



}
