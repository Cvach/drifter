﻿using UnityEngine;
using System.Collections;

public class JumpRing : Ability {

    public float speedIncrease = 50.0f;
    public float AccelIncrease = 30.0f;
    public float rotationSpeed = 200.0f;
    public Vector3 axis = new Vector3(0.0f, 0.0f, 1.0f);
    private static int hits = 0;
    PlayerMovement player;
    
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    public override void Activate()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>()!=null)
        {
            SoundManager.Instance.Play("Bloop");
            viewInfo(ref hits);
            player = other.GetComponent<PlayerMovement>();
            player.playerSpeed = player.maxSpeed + speedIncrease;
            //135.
            //With 5 up...
            //Meaning 135 over and 5 up would do it.
            Vector3 vel = player.GetComponent<Rigidbody>().velocity;
            vel.y = 15.0f;
            vel.x = player.playerSpeed;

            player.GetComponent<Rigidbody>().velocity = (vel);

            player.DisableGravity(1.0f);
            player.DisableDecel(1.0f);

            Destroy(this.gameObject);
        }
    }


}