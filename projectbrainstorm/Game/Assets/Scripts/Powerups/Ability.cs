﻿using UnityEngine;
using System.Collections;

public abstract class Ability : MonoBehaviour {

    public float remainingDuration;
    public float startDuration = 5.0f;
    private static readonly int views = 3;
    public bool active = false;
    public string descript;

    public abstract void Activate();

    protected void viewInfo(ref int hits)
    {
        if (hits < views)
        {
            hits++;
            GameObject.Find("PowerupInformationText").GetComponent<PowerupInfo>().setText(descript);
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

}
