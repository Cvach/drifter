﻿using UnityEngine;
using System.Collections;

public class ScoreMutliplierBoost : Ability {
    private static int hits = 0;
    public int multiplierBonus = 1; 
	// Use this for initialization
	void Start () {
        descript = "Increases score multiplier!!";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Activate()
    {
        //throw new System.NotImplementedException();
    }

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            SoundManager.Instance.Play("Bloop");
            viewInfo(ref hits);
            Debug.Log("Multiplier Before: " + ScoreManager.GetMultiplier());
            ScoreManager.AddToScoreMultiplier(multiplierBonus);
            Debug.Log("Multiplier After: " + ScoreManager.GetMultiplier());

            Destroy(this.gameObject); 
        }
    }
}
