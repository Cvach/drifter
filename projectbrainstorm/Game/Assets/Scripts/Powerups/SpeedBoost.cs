﻿using UnityEngine;
using System.Collections;

public class SpeedBoost : Ability {
    private static int hits = 0;
    public float speedIncrease = 3.0f;
    public float rotationSpeed = 200.0f;
    public Vector3 axis = new Vector3(0.0f, 0.0f, 1.0f);
    PlayerMovement player;
    LerpToPlace lerper;


    
	// Use this for initialization
	void Start () {
        
        lerper = this.gameObject.GetComponent<LerpToPlace>();
	}
	
	// Update is called once per frame
	void Update () {
        if (lerper != null)
        {
            if (lerper.isDone())
            {
                ScoreManager.AddToScoreMultiplier(1);
                
                
                this.gameObject.SetActive(false); 
                Destroy(this.gameObject);
            }
            else
            {
                this.transform.Rotate(axis, rotationSpeed * Time.deltaTime);
            }
        }
	}

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    public override void Activate()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>()!=null)
        {
            this.transform.parent = other.gameObject.transform;
            viewInfo(ref hits);

            SoundManager.Instance.Play("Bloop");


            this.gameObject.GetComponent<Collider>().enabled = false;

            if (lerper != null)
            {
                lerper.activate();
            }


            /*
            player = other.GetComponent<PlayerMovement>();
            player.maxSpeed += speedIncrease * startDuration;
            player.playerAccel += speedIncrease;
            this.gameObject.transform.parent = other.gameObject.transform;
            this.gameObject.renderer.enabled = false;
            this.active = true;
            */
        }
    }


}