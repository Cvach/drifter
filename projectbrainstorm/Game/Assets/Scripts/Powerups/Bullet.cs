﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float maxDistance = 50.0f;
    public Vector3 startPos;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if ((this.gameObject.transform.position - this.startPos).sqrMagnitude > maxDistance * maxDistance)
        {
            Destroy(this.gameObject);
        }
	}

    void OnCollision(Collision other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }

    }

}
