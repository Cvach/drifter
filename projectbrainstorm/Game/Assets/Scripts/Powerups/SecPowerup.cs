﻿using UnityEngine;
using System.Collections.Generic;

public class SecPowerup : MonoBehaviour {

    public float chance;
    public List<GameObject> powerups = new List<GameObject>();
    // Use this for initialization
    void Start () {
        if (Random.value < chance)
        {
            //Choose a random powerup from the list.
            int ran = Random.Range(0, powerups.Count);
            ((GameObject)Instantiate(powerups[ran], transform.position, powerups[ran].transform.rotation)).transform.parent = this.transform.parent;
        }
        Destroy(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
