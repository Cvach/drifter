﻿using UnityEngine;
using System.Collections;

public class Shield : Ability {

    GameObject p;
    public Mesh meshChanger;
    public float scrollSpeed = 0.05f;
    private float remaningDuration = 0;
    private static int hits = 0;

    private Vector2 meDirection;
	// Use this for initialization
	void Start () {
	    meDirection = Vector3.up/5.0f;
        
	}
	
	// Update is called once per frame
	void Update () {
        float offset = Time.time * scrollSpeed;
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", meDirection * offset);
        if (active)
        {
            if (remainingDuration < -5.0f)
            {
                remainingDuration = 5.0f;
            }
            remainingDuration += Time.deltaTime;

            if (Mathf.Abs(remainingDuration - startDuration) < 5)
            {
                Color newColor = gameObject.GetComponent<Renderer>().material.color;
                newColor.g -= Time.deltaTime / 5;
                newColor.b -= Time.deltaTime / 5;
                newColor.a += Time.deltaTime / 60;
                scrollSpeed += Time.deltaTime / 10;
                gameObject.GetComponent<Renderer>().material.color = newColor;
            }

            if (remainingDuration > startDuration)
            {
                Destroy(gameObject);
            }
        }
	}

    public override void Activate()
    {
        
    }

    public static void ResetAbilityHints()
    {
        hits = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        //print("Picked up sheild");
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            if (other.gameObject.transform.Find("Bike").Find("Shield(Clone)"))
            {
                print("increase duration of shield");
                other.gameObject.transform.Find("Bike").Find("Shield(Clone)").GetComponent<Shield>().remainingDuration -= startDuration / 2;
            }
            else
            {
                SoundManager.Instance.Play("Bloop");
                p = other.GetComponent<PlayerMovement>().transform.FindChild("Bike").gameObject;
                this.gameObject.transform.position = p.transform.position;
                this.gameObject.transform.parent = p.transform;
                this.active = true;
                this.gameObject.GetComponent<MeshFilter>().mesh = meshChanger;
                viewInfo(ref hits);
            }

        }
        else
        {
            if (this.active&&other.gameObject.tag=="Obstacle")
            {
                Destroy(other.gameObject);
                
            }
        }

    }




}
