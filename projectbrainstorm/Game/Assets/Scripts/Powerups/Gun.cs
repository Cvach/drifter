﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Gun :Ability {

    PlayerMovement p;
    public float timeBetweenShots = 0.2f;
    float timeSinceLastShot = 1.0f;
    public float lookRadius = 3.0f;
    public float gunRange = 2000.0f;
    public ParticleSystem particleToSpawn;
    private static int hits = 0;
	// Use this for initialization
	void Start () {
        startDuration = 5.0f; 
        this.remainingDuration = this.startDuration;
        this.active = false;
        descript = "A gun!! It fires where you're looking.";
	}

    public static void ResetAbilityHints()
    {
        hits = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (this.active)
        {
            if (remainingDuration > 0.0f)
            {
                
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > timeBetweenShots)
                {
                    shoot();
                }
                
                this.remainingDuration -= Time.deltaTime;
            }
            else
            {
                Destroy(this.gameObject);
            }

        }
	}

    private void shoot()
    {
        RaycastHit hit = new RaycastHit();
        Ray ray =new Ray();
        ray.direction = Controller.GetForward();
        ray.origin = Controller.GetPosition() + (ray.direction * lookRadius);

        transform.rotation = Quaternion.LookRotation(ray.direction);

        if (Physics.Raycast(ray, out hit, this.gunRange))
        {
            resolveCollision(hit);
        }
        else
        {
        }

        timeSinceLastShot = 0.0f; 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            this.active = true;
            p = other.GetComponent<PlayerMovement>();
            viewInfo(ref hits);
            this.gameObject.transform.position = p.getSteveForward();
            this.gameObject.transform.parent = p.transform;

            foreach (Transform aaa in this.gameObject.transform)
            {
                aaa.gameObject.GetComponent<Renderer>().enabled = false;
            }
            this.active = true;
            
            this.gameObject.transform.localRotation = new Quaternion(); 
            //this.gameObject.collider.active = false;
        }
    }

    private void resolveCollision(RaycastHit hit)
    {
        Debug.Log("Hit : " + hit.collider.gameObject.name);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.tag == "Obstacle")
            {
                transform.rotation = Quaternion.LookRotation(hit.point);

                Instantiate(particleToSpawn, hit.point, new Quaternion());
                Destroy(hit.collider.gameObject);
                ScoreManager.AddToScore(10);
                ScoreManager.flashScore();
               
            }
        }
    }







    public override void Activate()
    {
       
    }
}
