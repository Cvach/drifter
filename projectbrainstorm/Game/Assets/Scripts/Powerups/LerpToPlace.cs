﻿using UnityEngine;
using System.Collections;

public class LerpToPlace : MonoBehaviour {

    public Vector3 startDisplacement = new Vector3(1.5f,0.0f,0.0f);

    private enum States { active, done, idle }
    private States isActive = States.idle;
    private float percentThere = 0.0f;
    public static GameObject lerpTo;
    public static GameObject lerpFrom;
    public static GameObject lerpScoreMult;

    private float startScale;

    public bool isMult;
    

	// Use this for initialization
	void Start () {
        startScale = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
        if (isActive ==States.active)
        {
            if (percentThere < 1.0f)
            {

                percentThere += Time.deltaTime;

                this.gameObject.transform.position = Vector3.Lerp(lerpFrom.transform.position + startDisplacement, 
                    (!isMult? lerpTo : lerpScoreMult).gameObject.transform.position, percentThere);

                /*
                float sc = startScale + (startScale * percentThere);
                transform.localScale = new Vector3(sc, sc, sc);
                 * */
                //Debug.Log("I'M LERPING : " + percentThere);
            }
            else
            {
                isActive = States.done;
            }

        }
	}


    public bool isDone()
    {
        return isActive == States.done;
    }


    public void activate()
    {
        isActive = States.active;
    }




}
