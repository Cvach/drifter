﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerMovement : MonoBehaviour 
{
    //public GameObject startPoint;
    public float playerSpeed;
    float engineSpeed, engineAccel;
    float maxEngineAccel = 50.0f;
    public float maxSpeed;
    public float playerAccel;
    public float minAccel;
    public float bigMaxSpeed;

    float maxEngineSpeed = 130.0f;

    private float startY;
    public float grav;
    private float vertVel = 0;

    private Controller controller;
    public Vector3 spawnPoint;
    public float startDelay;
    private float startTimer, postDeathTimer, postDeathDelay;
    public float terminalVel;
    private bool isInvincible, isViewingPostDeathScreen;
    private float velTwoFramesAgo = 0;
	private float invincibilityTimer, velLastFrame, minVelDeficit;

    private float originalMaxSpeed;

    public bool isBoosted = false;

    public float decel;
    private float decelDisableTimer = 0.0f;
    private float lastSpeed;

    private float startFOV;
    private float fovScale = 0.4f;
    private float fovUpSpeed = 120.0f;

    private float gravDisableTimer = 0.0f;

    public float playerJerk;

    private int invincibleFrames = 0;

    float[] speeds = new float[3];



    float lastYVel = 0.0f;



    public float getEngineSpeedForSound()
    {
        return engineSpeed;
    }

    Camera[] cams;

	// Use this for initialization
	void Start () 
    {
        //bike = transform.Find("Bike").gameObject;
        JumpRingCombo.player = this.gameObject;
        LerpToPlace.lerpFrom = transform.Find("Bike").gameObject;
        LerpToPlace.lerpTo = transform.Find("WorldCanvas").Find("ScoreText").gameObject;
        LerpToPlace.lerpScoreMult = transform.Find("WorldCanvas").Find("Multiplier").gameObject;

        cams = this.transform.root.GetComponentsInChildren<Camera>().Where(c=>c.gameObject.name != "Camera").ToArray();

        foreach (Camera c in cams)
        {
            c.transparencySortMode = TransparencySortMode.Orthographic;
        }
        //controller = this.gameObject.GetComponent<Controller>();
        startTimer = 0;
        invincibilityTimer = 0;
        minVelDeficit = .8f;
        velLastFrame = 0;
        postDeathTimer = postDeathDelay = 4.0f;
        isViewingPostDeathScreen = false;
        isInvincible = false;
       
        OVRManager.display.RecenterPose();
        Controller.SetPosition(spawnPoint);
        ScoreManager.Init();
        startY = transform.position.y;
        originalMaxSpeed = maxSpeed;


        for (int i = 0; i < speeds.Length; i++)
        {
            speeds[i] = (i + 1) * maxSpeed / (speeds.Length + 1);
        }
    }
	
    public float getMaxEngineSpeed()
    {
        return maxEngineSpeed;
    }
    void EngineNoiseUpdate()
    {
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.y - lastYVel) > 2.0f)
        {
            engineSpeed *= 0.95f;
        }
        lastYVel = GetComponent<Rigidbody>().velocity.y;


        float jerk = (playerSpeed - engineSpeed) * 0.5f;
        if (Mathf.Abs(jerk) < 1.0f)
        {
            //jerk = 0;
        }
        engineAccel += jerk * Time.deltaTime;

        //Test
        engineAccel = (playerSpeed - engineSpeed) * 0.2f;
        if (engineAccel > maxEngineAccel)
        {
            engineAccel = maxEngineAccel;
        }
        if (-engineAccel > maxEngineAccel)
        {
            engineAccel = -maxEngineAccel;
        }

        engineSpeed += engineAccel * Time.deltaTime;
        engineSpeed = engineSpeed > maxEngineSpeed ? maxEngineSpeed : engineSpeed;
        if (engineSpeed < 0)
        {
            engineSpeed = 0;
        }

        /*
        foreach (float f in speeds)
        {
            if (playerSpeed > f && lastSpeed < f)
            {
                engineSpeed *= 0.5f;
            }
        }
        */

        if (engineSpeed > (playerSpeed - 5))
        {
            engineSpeed *= 0.8f;
        }


        lastSpeed = engineSpeed;
    }

    void FixedUpdate()
    {
        CompleteVelocityChecks();
        Vector3 ve = GetComponent<Rigidbody>().velocity;
        if (ve.z > ve.y)
        {
            ve.z = ve.y;
        }
        
        if (isViewingPostDeathScreen)
        {
            postDeathTimer += Time.fixedDeltaTime;
            playerSpeed = 0;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            isViewingPostDeathScreen = (postDeathTimer <= postDeathDelay);
            if (!isViewingPostDeathScreen)
            {
                Reset();
            }
        }
        else if (startTimer >= startDelay)
        {
            if (!ScoreManager.IsTrackingScore())
            {
                ScoreManager.StartTrackingScore();
            }
            MoveLeftOrRight();
        }
        else
        {
            Controller.SetPosition(spawnPoint);
            playerSpeed = 0;
            if (startTimer < startDelay / 2.5f)
            {
                OVRManager.display.RecenterPose();
            }
        }
    }


    /*
    //private float bikeAngle = 0.0f;
    private float destAngle;
    private GameObject bike;

    private bool skipAngle = false;
    void OnCollisionEnter(Collision c)
    {
        if (c.collider.gameObject.name == "Floor")
        {
            skipAngle = true;
        }
        else
        {
            Vector3 norm = (c.contacts[0].normal);
            destAngle = Mathf.Rad2Deg * Quaternion.Angle(Quaternion.Euler(norm), Quaternion.Euler(new Vector3(0, 1, 0)));
            skipAngle = false;
        }
        //I think that angle is what we want rotation.euler.x to be... Maybe.
    }

    void AngleBike()
    {
        if (skipAngle)
            return;

        Quaternion newQuat = Quaternion.Euler(bike.transform.localRotation.eulerAngles.x, bike.transform.localRotation.eulerAngles.y, destAngle);

        Quaternion newRot = Quaternion.RotateTowards(bike.transform.localRotation, newQuat, 100 * Time.deltaTime);
        //Debug.Log(destAngle);
        bike.transform.localRotation = newRot;
    }
    */
    int updateCount = 0;
    // Update is called once per frame
    void Update () 
    {

        //AngleBike();

        EngineNoiseUpdate();
        updateCount++;

        if (updateCount == 2)
        {
            startFOV = cams[1].fov;
            Debug.Log("Set start FOV to " + startFOV + " from " + cams[1].gameObject.name);
        }
        if (updateCount >= 2)
        {
            modFOV();
        }

        ScoreManager.MyUpdate();

        if (Controller.isTapped)
        {
            viewPostDeath();
        }

        if (Input.GetKey(KeyCode.C))
        {
            ScoreManager.ClearHighScores();
        }

        
        /*
        if (Input.GetKeyDown(KeyCode.F))
        {
            DoThatJumpThing();
        }
        */
        //if (isInvincible)
        //{
        //    invincibilityTimer -= Time.deltaTime;
        //    isInvincible = invincibilityTimer <= 0;
        //}

		
        
        startTimer += Time.deltaTime;
        /*
        if (vertVel != 0)
        {
            vertVel += grav * Time.deltaTime;
            Vector3 pos = transform.position;
            if (pos.y > startY || vertVel > 0)
            {
                pos.y += vertVel;
            }
            if (pos.y <= startY)
            {
                vertVel = 0;
                pos.y = startY;
            }
            transform.position = pos;
        }
        */
        //ScoreManager.SetMultiplier((int)(playerSpeed / 20) + 1);


        if (gravDisableTimer > 0)
        {

            gravDisableTimer -= Time.deltaTime;
            if (gravDisableTimer <= 0)
            {
                this.GetComponent<Rigidbody>().useGravity = true;
            }
        }
	}

    public void DisableGravity(float time)
    {
        gravDisableTimer = time;
        GetComponent<Rigidbody>().useGravity = false;
    }
    public void DisableDecel(float time)
    {
        decelDisableTimer = time;
        
    }

    public int getSecondsOfDelayLeft()
    {
        int result = (int)Mathf.Ceil(startDelay - startTimer);
        return result;
    }

    public Vector3 getForward()
    {
        Vector3 resultant = gameObject.transform.position;
        resultant.z += gameObject.transform.FindChild("Bike").GetComponent<Renderer>().bounds.size.z / 2 + .25f;
        return resultant;
    }
    public Vector3 getDrewForward()
    {
        Vector3 resultant = gameObject.transform.position;
        resultant.z += gameObject.transform.FindChild("Bike").GetComponent<Renderer>().bounds.size.z / 2 + 1.0f;
        return resultant;
    }
    public Vector3 getSteveForward()
    {
        Vector3 resultant = gameObject.transform.position;
        resultant.z += gameObject.transform.FindChild("Bike").GetComponent<Renderer>().bounds.size.z / 2 + .8f;
        resultant.y -= .8f;
        return resultant;
    }
	
	private void CompleteVelocityChecks()
	{
        Vector3 myVel = GetComponent<Rigidbody>().velocity;
        if (myVel.y < terminalVel)
        {
            myVel.y = terminalVel;
        }

        GetComponent<Rigidbody>().velocity = myVel;
        
        if (--invincibleFrames <= 0 && startTimer > startDelay + 2 && gameObject.GetComponent<Rigidbody>().velocity.z < 0.1f && !isViewingPostDeathScreen)
        {
            print("Drew's janky death check. " + gameObject.GetComponent<Rigidbody>().velocity.z);
            //Reset();
            viewPostDeath();
        }
	}

    public void setInvincibility(float lengthOfTime)
    {
        isInvincible = true;
    }
    
    void OnCollisionEnter(Collision col)
    {
        if (lastYVel < -1 && col.collider.tag == "Ramp")
        {
            Debug.Log("Invincible for next 5 frames");
            invincibleFrames = 5;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle" && !other.GetComponent<Collider>().isTrigger)
        {
            //viewPostDeath();
        }
        //Reset();
    }

    void MoveLeftOrRight()
    {
        //if (playerAccel > minAccel)
        //{
        //    playerAccel += playerJerk * Time.deltaTime;
        //}
        //else
        //{
        //    playerAccel = minAccel;
        //}
        if (playerSpeed > bigMaxSpeed)
        {
            playerSpeed = bigMaxSpeed;
        }
        if (playerSpeed < maxSpeed && playerSpeed < bigMaxSpeed)
        {
            playerSpeed += playerAccel * Time.deltaTime;
        }
        else
        {
            if (decelDisableTimer <= 0)
            {
                playerSpeed -= decel * Time.deltaTime;
            }
            else
            {
                decelDisableTimer -= Time.deltaTime;
            }
        }

        Vector3 vel = new Vector3(Controller.GetTranslationXAxis() * playerSpeed, GetComponent<Rigidbody>().velocity.y, playerSpeed);
        vel.x = (Mathf.Abs(vel.x) > 100) ? 0 : vel.x;

        /*Debug key codes*/
        if (Input.GetKey(KeyCode.A))
        {
            vel = new Vector3(-1000 * Time.deltaTime, GetComponent<Rigidbody>().velocity.y, playerSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            vel = new Vector3(1000 * Time.deltaTime, GetComponent<Rigidbody>().velocity.y, playerSpeed);
        }

        GetComponent<Rigidbody>().velocity = vel;

        //Vector3 movementThisFrame = vel * Time.deltaTime;
        //gameObject.transform.position += movementThisFrame;
        //camera.AddToPosition(movementThisFrame);

        //rigidbody.velocity = Vector3.zero;
    }

    void DoThatJumpThing()
    {
        vertVel = 3.0f * playerSpeed/(maxSpeed*0.95f);
        GetComponent<Rigidbody>().velocity += new Vector3(0, 3.0f * playerSpeed / (maxSpeed * 0.95f), 0);
    }

    public void resetMaxSpeed()
    {
        maxSpeed = originalMaxSpeed;
    }

    public void AddToVelocity(Vector3 velToAdd)
    {
        GetComponent<Rigidbody>().velocity += velToAdd;
    }

    private void viewPostDeath()
    {
        foreach (AudioSource a in GameObject.Find("Bike").GetComponents<AudioSource>())
        {
            a.Stop();
        }

        isViewingPostDeathScreen = true;
        postDeathTimer = 0;
        gameObject.transform.position = new Vector3(spawnPoint.x, spawnPoint.y - 9002.0f, spawnPoint.z);
        GetComponent<Rigidbody>().useGravity = false;
        ScoreManager.StopTrackingScore();
    }
    
    public bool getIsViewingPostDeath()
    {
    	return isViewingPostDeathScreen;
    }

    void Reset()
    {
        Application.LoadLevel(Application.loadedLevelName);
        GetComponent<Rigidbody>().useGravity = true;
        /*
        foreach ( AudioSource a in GameObject.Find("Bike").GetComponents<AudioSource>())
        {
            a.Play();
        }

        playerSpeed = 20;
        velLastFrame = 0;
        velTwoFramesAgo = 0;
        isViewingPostDeathScreen = false;
        this.gameObject.transform.position = spawnPoint;
        Controller.SetPosition(spawnPoint);
        OVRManager.display.RecenterPose();
        GameObject.Find("Bike").transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, 1));
        GameObject.Find("Bike").transform.localRotation = Quaternion.LookRotation(new Vector3(0, 0, 1));
        startTimer = 0;
         */
        ScoreManager.AttemptSetHighScore(ScoreManager.GetCurrentScore());
        ScoreManager.StopTrackingScore();
        ScoreManager.ResetMultiplier();
        ScoreManager.ResetScore();
        engineSpeed = 0.0f;
        resetAbilityHints();
        
    }

    void resetAbilityHints()
    {
        SpeedBoost.ResetAbilityHints();
        ScoreMutliplierBoost.ResetAbilityHints();
        ScoreBoost.ResetAbilityHints();
        Shield.ResetAbilityHints();
        NukeLander.ResetAbilityHints();
        Gun.ResetAbilityHints();
        JumpRing.ResetAbilityHints();
    }

    void modFOV()
    {
        if (playerSpeed > maxSpeed + 3f)
        {
            float targetFOV = (playerSpeed - maxSpeed) * fovScale + startFOV;
            
            foreach (Camera c in cams)
            {
                float curFOV = c.fieldOfView;
                if (targetFOV > curFOV)
                {
                    c.fieldOfView = curFOV + fovUpSpeed * Time.deltaTime;
                }
                else
                {
                    c.fieldOfView = targetFOV;
                }
            }
        }
        else
        {
            /*
            foreach (Camera c in cams)
            {
                c.fieldOfView = startFOV;
            }
             * */
        }
    }

    /*
    float fastFOVMod = 30;
    bool goingFast = false;
    float startFOV;
    float fovShiftSpeed = 1;
    IEnumerator GottaGoFast()
    {
        if (!goingFast)
        {
            goingFast = true;
            startFOV = Camera.allCameras[0].fieldOfView;
            float fastFOV = 90;
            for (float f = startFOV; f < fastFOV; f += fovShiftSpeed * fastFOVMod * Time.deltaTime)
            {
                if (f > fastFOV)
                {
                    f = fastFOV;
                }
                foreach (Camera c in Camera.allCameras)
                {
                    c.fieldOfView = f;
                }
                yield return null;
            }
        }
        
    }
     * */
}
