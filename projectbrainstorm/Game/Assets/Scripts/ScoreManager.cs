﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ScoreManager 
{

    private static float score;
    private static int highScore;
    private static int scoreMultiplier;
    private static int multiplierLastReturned;
    private static bool isTrackingScore = false;
    private static List<int> highScores = new List<int>();
    private static int MAXHIGHSCORES = 10;
    private static float flashTimer = 0;
    private static float flashDuration = .5f;
    private static bool isFlashing = false;


    public static void Init()
    {
        SetMultiplier(1);
        InitHighScores();
    }
    public static void MyUpdate()
    {
        WriteHighScores();
        if (isTrackingScore)
        {
            AddToScore(Time.deltaTime * scoreMultiplier);
        }
        
        writeScore();
        writeLeaderboard();

        if (isFlashing)
        {
            flashTimer += Time.deltaTime;
        }

        if (flashTimer > flashDuration)
        {
            GameObject.Find("Multiplier").GetComponent<UnityEngine.UI.Text>().color = new Color(1, 1, 1, 1);
            isFlashing = false;
            flashTimer = 0;
        }
    }

    public static void flashScore()
    {
        Flasher f = Transform.FindObjectOfType<Flasher>();
        f.Flash();
    }

    public static void flashMultiplier()
    {
        GameObject.Find("Multiplier").GetComponent<UnityEngine.UI.Text>().color = new Color(1, 0, 0, 1);
        isFlashing = true;
    }

    private static void writeScore()
    {
        GameObject scoreText = GameObject.Find("ScoreText");
		string prevText = (GameObject.Find("OVRCameraRig").gameObject.transform.GetComponent<PlayerMovement>().getIsViewingPostDeath())?"Your Score Was: ":"";
        ((UnityEngine.UI.Text)scoreText.GetComponent<UnityEngine.UI.Text>()).text = prevText + GetCurrentScore();
    }

    private static void writeLeaderboard()
    {
        GameObject leftLeaderBoardText = GameObject.Find("LeaderBoardLeft");
        GameObject rightLeaderBoardText = GameObject.Find("LeaderBoardRight");
        
        string leaderBoardString = "Leader Board\n";
        UpdateHighScores();
        for (int i = 0; i < MAXHIGHSCORES && i < highScores.Count; i++)
        {
            if (i < 9)
            {
                leaderBoardString += " " + (i + 1) + ") " + highScores[i] + " \n";
            }
            else
            {
                leaderBoardString += (i + 1) + ") " + highScores[i] + " \n";
            }
        }
        ((UnityEngine.UI.Text)leftLeaderBoardText.GetComponent<UnityEngine.UI.Text>()).text = leaderBoardString;
        ((UnityEngine.UI.Text)rightLeaderBoardText.GetComponent<UnityEngine.UI.Text>()).text = leaderBoardString;
    }

    private static void InitHighScores()
    {
        for (int i = 0; i < MAXHIGHSCORES; i++)
        {
            highScores.Add(PlayerPrefs.GetInt("highScore" + i));
        }
    }

    private static void UpdateHighScores()
    {
        for (int i = 0; i < highScores.Count; i++)
        {
            highScores[i] = PlayerPrefs.GetInt("highScore" + i);
        }
    }

    public static List<int> GetHighScores()
    {
        UpdateHighScores();
        return new List<int>(highScores);
    }

    public static void ClearHighScores()
    {
        for (int i = 0; i < MAXHIGHSCORES; i++)
        {
            PlayerPrefs.SetInt("highScore" + i, 0);
        }
    }

    private static void WriteHighScores()
    {
        for (int i = 0; i < highScores.Count; i++)
        {
            PlayerPrefs.SetInt("highScore" + i, highScores[i]);
        }

        PlayerPrefs.Save();
    }


    public static void printList<T>(this List<T> me)
    {
        string s = "";
        foreach (T t in me)
        {
            s += t.ToString() + ", ";
        }
        Debug.Log(s);
    }
    
    public static void AttemptSetHighScore(int newHighScore)
    {
        UpdateHighScores();


        highScores.Add(newHighScore);
        highScores.Sort();
        highScores.Reverse();
        while (highScores.Count > MAXHIGHSCORES)
        {
            highScores.RemoveAt(highScores.Count - 1);
        }
        //highScores.printList();
        WriteHighScores();
    }

    public static void printScoreTest()
    {
        UpdateHighScores();
        string stringToPrint = "";

        for (int i = 0; i < highScores.Count; i++)
        {
            stringToPrint += i + ": " + highScores[i] + ", ";
        }

        Debug.Log(stringToPrint);
    }

    public static bool IsTrackingScore()
    {
        return isTrackingScore;
    }

    public static void StartTrackingScore()
    {
        isTrackingScore = true;
    }

    public static void StopTrackingScore()
    {
        isTrackingScore = false;
    }

    public static void AddToScoreMultiplier(int amountToAdd)
    {
        flashMultiplier();
        scoreMultiplier += amountToAdd;
    }

    public static void RemoveFromScoreMultiplier(int amountToRemove)
    {
        scoreMultiplier -= amountToRemove;
    }

    public static void SetMultiplier(int newMultiplier)
    {
        scoreMultiplier = newMultiplier;
    }

    public static int GetMultiplier()
    {
        multiplierLastReturned = scoreMultiplier;
        return scoreMultiplier;
    }

    public static bool HasMultiplierChanged()
    {
        return multiplierLastReturned != scoreMultiplier;
    }

    public static void ResetMultiplier()
    {
        scoreMultiplier = 1;
    }

    public static void AddToScore(float pointsToAdd)
    {
        score += pointsToAdd;
    }

    public static void RemoveFromScore(float pointsToRemove)
    {
        score -= pointsToRemove;
    }

    public static void ResetScore()
    {
        score = 0;
    }

    public static int GetCurrentScore()
    {
        return (int)score;
    }
}
