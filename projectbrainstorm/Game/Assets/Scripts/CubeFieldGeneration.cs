﻿using UnityEngine;
using System.Collections;

public class CubeFieldGeneration : MonoBehaviour {

    public int numToSpawn = 5;
    public int numPortion = 1; 
    public GameObject toSpawn;
    public GameObject planeToSpawnOn;
    private float coinChance = 0.1f;
    private float multiplierChance = 0.02f;

    public GameObject coin;
    public GameObject multiplier;


	// Use this for initialization
	void Start () {
        generateField();
	}

    void generateField()
    {

        int diff = transform.root.GetComponent<Section>().diff;

        numToSpawn = (diff < 2 ? 0 : (diff < 60 ? (int)(0.3f * (diff)) : 20));

        Vector3 spot = planeToSpawnOn.gameObject.transform.position;
        Vector3 portionPos = planeToSpawnOn.transform.position;
        float sizeOfPlane = planeToSpawnOn.transform.localScale.z; 
        float sizeOfPortion = (sizeOfPlane/numPortion);
        portionPos = new Vector3(portionPos.x,portionPos.y, (spot.z + sizeOfPlane/2) - (sizeOfPortion/2)); 

        for (int j = 0; j < numPortion; j++)
        {
            for (int i = 0; i < numToSpawn; i++)
            {
                float xSpawn = Random.Range(-(planeToSpawnOn.transform.localScale.x/2), + (planeToSpawnOn.transform.localScale.x/2));
                float zSpawn = Random.Range(-sizeOfPortion/2, sizeOfPortion/2);
                float ySpawn = 2;

                float val = Random.value;
                bool co = val < coinChance;
                if (co) ySpawn = 2.5f;
                bool mult = val < coinChance + multiplierChance;
                GameObject tos = co ? coin : (mult? multiplier: toSpawn);
                GameObject spawned = (Instantiate(tos, planeToSpawnOn.transform.position + new Vector3(xSpawn, ySpawn, zSpawn), tos.transform.rotation) as GameObject);
                spawned.transform.parent = planeToSpawnOn.transform;
                if (co)
                {
                    Vector3 nt = spawned.transform.position;
                    nt.y -= 1.0f;
                    spawned.transform.position = nt;
                }
            }
            portionPos = new Vector3(portionPos.x,portionPos.y, portionPos.z - (sizeOfPortion));
        }
    }


	
	// Update is called once per frame
	void Update () {
	
	}
}
