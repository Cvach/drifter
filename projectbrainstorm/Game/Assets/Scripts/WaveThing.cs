﻿using UnityEngine;
using System.Collections;

public class WaveThing : MonoBehaviour {

    private float startingX;
    private float offset;
    private float offsetTracker;
    private float magnitude = 40;
    public bool isMovingRight = true;
    public float moveSpeed;
	// Use this for initialization
	void Start () 
    {
        startingX = transform.position.x;
        moveSpeed = isMovingRight ? moveSpeed : -moveSpeed;
	}
	
	// Update is called once per frame
	void Update () 
    {

        offset = (Mathf.Sin(offsetTracker) * magnitude) + startingX;
        Vector3 newPos = transform.position;
        newPos.x = offset;
        transform.position = newPos;

        float difficulty = Mathf.Clamp((float)transform.root.GetComponentInChildren<Section>().diff / 500.0f, .15f, 1.2f);
        offsetTracker += moveSpeed * Time.deltaTime * difficulty;
	}
}
