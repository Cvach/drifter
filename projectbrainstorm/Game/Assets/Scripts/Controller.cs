﻿using UnityEngine;
using System.Collections;
using System;

public class Controller : MonoBehaviour 
{
    public Action PlayerNodded;
    public Action PlayerShookNo;
    private bool isLookingForNod = false;

    [HideInInspector]
    public static bool isTapped;

    private static float tapThreshold = 150;
    private static float tapDelay = 0.1f;
    private static float tapRest;
    // Use this for initialization
    void Start()
    {
        //OVRManager.instance

    }

    // Update is called once per frame
    void Update()
    {
        LeanBike();

        isTapped = false;
        float deltaAccel;

        Ovr.Vector3f ang = OVRManager.capiHmd.GetTrackingState().HeadPose.AngularAcceleration;
        deltaAccel = Mathf.Abs(ang.x) + Mathf.Abs(ang.y) + Mathf.Abs(ang.z);

        tapRest -= Time.deltaTime;

        if (deltaAccel > tapThreshold && tapRest <= 0)
        {
            isTapped = true;
            tapRest = tapDelay;

            //Do the stuff you want for tapping
        }
    }

    public static void SetPosition(Vector3 newPosition)
    {
        GameObject theCamera = GameObject.Find("OVRCameraRig");
        theCamera.transform.position = newPosition;

    }

    private bool nodStarted = false;
    private float nodTimer = 0;
    private void LookForNod()
    {
        if (nodStarted)
        {
            nodTimer += Time.deltaTime;
        }

        if ((GetForward() - Vector3.forward).magnitude <= 2)
        {
            nodStarted = true;
        }
        else
        {
            nodStarted = false;
        }

        if (GetUp().z + (-GetForward().y) > .5f)
        {
            if (nodTimer < .5f)
            {
                //PlayerNodded.Invoke();
            }
        }
        
    }

    private void LookForShake()
    {
        //PlayerShookNo.Invoke();
    }

    public static void AddToPosition(Vector3 vectorToAdd)
    {
        GameObject theCamera = GameObject.Find("OVRCameraRig");
        theCamera.transform.position = theCamera.transform.position + vectorToAdd;
    }

    public static Vector3 GetUp()
    {
        Vector3 upVector;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        upVector = orientationReference.transform.rotation * new Vector3(0, 1, 0);
        return upVector;
    }

    public static Vector3 GetRight()
    {
        Vector3 rightVector;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        rightVector = orientationReference.transform.rotation * new Vector3(1, 0, 0);
        return rightVector;
    }

    public static Vector3 GetForward()
    {
        Vector3 forwardVector;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        forwardVector = orientationReference.transform.rotation * new Vector3(0, 0, 1);
        return forwardVector;
    }

    public static Vector3 GetPosition()
    {
        return GameObject.Find("CenterEyeAnchor").transform.position;
    }

    public static float GetTranslationXAxis()
    {
        float xTranslation;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        xTranslation = orientationReference.transform.localPosition.x;

        return xTranslation;
    }

    public static float GetTranslationYAxis()
    {
        float yTranslation;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        yTranslation = orientationReference.transform.localPosition.y;
        return yTranslation;
    }

    public static float GetTranslationZAxis()
    {
        float zTranslation;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        zTranslation = orientationReference.transform.localPosition.z;
        return zTranslation;
    }

    private void LeanBike()
    {
        float xTranslation;
        GameObject orientationReference = GameObject.Find("CenterEyeAnchor");
        xTranslation = orientationReference.transform.localPosition.x;

        GameObject bike = GameObject.Find("Bike");
        bike.transform.localRotation = Quaternion.Euler(new Vector3(-xTranslation * 45, 270, bike.transform.localRotation.z));
    }

    public void ResetView()
    {
        
    }
}
