﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CalibrateText : MonoBehaviour {
    public float distance;
    private bool calibrating = true;
	// Use this for initialization
	void Start () {
        distance = (distance < 1) ? 1 : distance;
        
	}
	
	// Update is called once per frame
	void Update () {
        int timeLeft =  gameObject.transform.root.GetComponent<PlayerMovement>().getSecondsOfDelayLeft();
        if (timeLeft > 4)
        {
            calibrating = true;
            Text obj = gameObject.transform.GetComponent<Text>();
            obj.enabled = true;
            RotateToView();
            gameObject.transform.GetComponent<Text>().text = "Calibrating...\n";// +(timeLeft - 4);
        }
        else if (timeLeft > 3)
        {
            calibrating = true;
            Text obj = gameObject.transform.GetComponent<Text>();
            obj.enabled = true;
            RotateToView();
            gameObject.transform.GetComponent<Text>().text = "Get Ready!";
        }
        else if (timeLeft > 0)
        {
            calibrating = false;
            Text obj = gameObject.transform.GetComponent<Text>();
            obj.enabled = true;
            RotateToView();
            gameObject.transform.GetComponent<Text>().text = "Starting in " + timeLeft;
        }
        else
        {
            gameObject.transform.GetComponent<Text>().enabled = false;
        }
	}

    void RotateToView()
    {
        Camera cam = gameObject.transform.root.FindChild("LeftEyeAnchor").GetComponent<Camera>();
        gameObject.transform.rotation = cam.transform.rotation;
        //print(cam.transform.forward.normalized);
        gameObject.transform.position = cam.gameObject.transform.position + cam.transform.forward.normalized * distance;
        Vector3 pos = gameObject.transform.position;
        //gameObject.transform.position = new Vector3(pos.x, pos.y+5, pos.z);
    }
}
