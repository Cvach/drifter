﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GrabMultiplier : MonoBehaviour {
    private int startFontSize, fontDiff, lerpFontStart, startingFSPS;
    public int largestFontSize, largestMultiplier, fontSizePerSec;
    private float beta, betaIncrement;
    private bool isGettingBigger;
	// Use this for initialization
	void Start () {
        startFontSize = gameObject.transform.GetComponent<Text>().fontSize;
        isGettingBigger = true;
        fontDiff = largestFontSize - startFontSize;
        beta = 0;
        startingFSPS = fontSizePerSec;
        calcFontDiff();
        betaIncrement = fontSizePerSec/(float)fontDiff;
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.GetComponent<Text>().enabled = ScoreManager.IsTrackingScore();
        if (ScoreManager.HasMultiplierChanged())
        {
            int multiplier = ScoreManager.GetMultiplier();
            gameObject.transform.GetComponent<Text>().text = multiplier + "x";
        }
        PulsateTextOrWhateverYoudCallItYouKnowTheWholeThrobbingThingYeahThat();
	}

    void PulsateTextOrWhateverYoudCallItYouKnowTheWholeThrobbingThingYeahThat()
    {
        if (isGettingBigger)
        {
            beta += betaIncrement*Time.deltaTime;
            beta = (beta >= 1.0f) ? 1.0f : beta;
            int currSize = (int)(startFontSize + (fontDiff * beta));
            gameObject.transform.GetComponent<Text>().fontSize = currSize;
            if (beta == 1.0f)
            {
                isGettingBigger = false;
                fontDiff = startFontSize - currSize;
                lerpFontStart = currSize;
                beta = 0;
            }
        }
        else
        {
            beta += betaIncrement * Time.deltaTime;
            beta = (beta >= 1.0f) ? 1.0f : beta;
            int currSize = (int)(lerpFontStart + (fontDiff * beta));
            gameObject.transform.GetComponent<Text>().fontSize = currSize;
            if (beta == 1.0f)
            {
                isGettingBigger = true;
                calcFontDiff();
                beta = 0;
                betaIncrement = (float)fontSizePerSec / ((float)(fontDiff));
            }
        }
    }

    private void calcFontDiff()
    {
        int gap = largestFontSize - startFontSize;
        int multiplier = ScoreManager.GetMultiplier();
        if (multiplier == 0)
        {
            multiplier = 2;
        }
        float multDiff = (float)multiplier / (float)largestMultiplier;
        multDiff = (multDiff > 1.0f) ? 1.0f : multDiff;
        fontSizePerSec = (int)(startingFSPS*(multDiff*3));
        fontDiff = (int)(gap * multDiff);
    }
}
