﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {


    public List<AudioClip> clips;


    Dictionary<string, AudioClip> clipDic = new Dictionary<string, AudioClip>();

    static SoundManager inst = null;
    public static SoundManager Instance
    {
        get
        {
            if (inst == null)
            {
                inst = Transform.FindObjectOfType<SoundManager>();
            }
            return inst;
        }
    }

	// Use this for initialization
	void Start () {
	    foreach(AudioClip c in clips)
        {
            string newname = c.name;
            if (!clipDic.ContainsKey(newname))
            {
                clipDic.Add(newname, c);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	    foreach(AudioSource aus in GetComponents<AudioSource>())
        {
            if (!aus.isPlaying)
            {
                Destroy(aus);
            }
        }
	}

    public void Play(string clip)
    {
        if (clipDic.ContainsKey(clip))
        {
            AudioSource nas = this.gameObject.AddComponent<AudioSource>();
            nas.clip = clipDic[clip];
            nas.Play();
        }
    }
}
