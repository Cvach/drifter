﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerupInfo : MonoBehaviour {
    private float lifetime, currTime, opacity;
	// Use this for initialization
	void Start () {
        lifetime = 2.5f;
        opacity = 0.0f;
        currTime = 0;
        transform.GetComponent<Text>().enabled = false;
        transform.GetChild(0).GetComponent<Text>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (currTime > 0)
        {
            currTime -= Time.deltaTime;
            if(currTime < lifetime/2.0f)
            {
                opacity =  (currTime / (lifetime / 2.0f));
                Color color = new Color(0, 0, 0, opacity);
                transform.GetComponent<Text>().color = color;
                transform.GetChild(0).GetComponent<Text>().color = new Color(1, 1, 1, opacity);
            }
            
        }
	}

    public void setText(string text)
    {
        Text textObj = transform.GetComponent<Text>();
        textObj.text = text;
        textObj.enabled = true;
        opacity = 1.0f;
        textObj.color = new Color(0, 0, 0, opacity);
        Text textChild = textObj.transform.GetChild(0).GetComponent<Text>();
        textChild.text = text;
        textChild.enabled = true;
        textChild.color = new Color(1, 1, 1, opacity);
        currTime = lifetime;
    }


}
