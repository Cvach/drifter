﻿using UnityEngine;
using System.Collections;

public class GoToBikeCam : MonoBehaviour {


    GameObject bikeCam;
	// Use this for initialization
	void Start () {
        bikeCam = (GameObject)transform.root.FindChild("BikeCamPosition").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = bikeCam.transform.position;
	}
}
