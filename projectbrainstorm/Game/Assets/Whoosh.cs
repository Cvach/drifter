﻿using UnityEngine;
using System.Collections;

public class Whoosh : MonoBehaviour {


    void OnTriggerEnter(Collider col)
    {
        if (col.transform.gameObject.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
