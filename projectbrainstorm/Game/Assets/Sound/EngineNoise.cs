﻿using UnityEngine;
using System.Collections;

public class EngineNoise : MonoBehaviour {

    AudioSource aud;
    PlayerMovement pm;
    float defaultPitch = 0.1f;
    float maxPitch = 2;

    // Use this for initialization
    void Start () {
        aud = GetComponent<AudioSource>();
        pm = transform.parent.GetComponent<PlayerMovement>();


	}
	
	// Update is called once per frame
	void Update () {
        float vel = pm.getEngineSpeedForSound();

        float ms = pm.getMaxEngineSpeed();

        float pitchMod = vel / ms;

        float realPitch = defaultPitch + pitchMod * (maxPitch - defaultPitch);
        aud.pitch = realPitch;

	}
}
