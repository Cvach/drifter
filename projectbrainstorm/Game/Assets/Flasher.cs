﻿using UnityEngine;
using System.Collections;

public class Flasher : MonoBehaviour {

    // Use this for initialization

    private float brightAlpha = 0.7f;
    private float flashSpeed = 1.0f;
    private SpriteRenderer me;
	void Start () {
        me = GetComponent<SpriteRenderer>();
        Color oldColor = me.color;
        oldColor.a = 0;
        me.color = oldColor;
	}
	
	// Update is called once per frame
	void Update () {
        if (me.color.a > 0.0f)
        {
            Color oc = me.color;
            oc.a -= flashSpeed * Time.deltaTime;
            if (oc.a < 0)
            {
                oc.a = 0;
            }
            me.color = oc;
        }
	}

    public void Flash()
    {
        Color oc = me.color;
        oc.a = brightAlpha;
        me.color = oc;
    }
}
