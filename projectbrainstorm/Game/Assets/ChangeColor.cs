﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {

    private Color col;
    private float h, s, v;
    public float hueSpeed;
    private GameObject player;
    private float yOffset = 0;
    private float xOffset = 0;
    private float scrollModifier = 7.5f;
    private float straffModifier = 10.0f;
	// Use this for initialization
	void Start () {
        //col = renderer.material.color;
        //EditorGUIUtility.RGBToHSV(col, out h, out s, out v);
        player = GameObject.Find("OVRCameraRig");
	}
	
	// Update is called once per frame
	void Update () {
        //h += Time.deltaTime * hueSpeed;
        //if (h > 1)
        //{
        //    h = 0;
        //}
        //renderer.material.color = EditorGUIUtility.HSVToRGB(h, s, v);
        //(player.rigidbody.velocity.x/10)
        //(player.GetComponent<PlayerMovement>().playerSpeed/10)

        float yScollSpeed = ((player.GetComponent<PlayerMovement>().playerSpeed / player.GetComponent<PlayerMovement>().maxSpeed) * scrollModifier);
        yOffset += yScollSpeed * Time.deltaTime;

        float xScrollSpeed = player.GetComponent<Rigidbody>().velocity.x / straffModifier;
        xOffset += xScrollSpeed * Time.deltaTime;

        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(xOffset, yOffset));
	}
}
