﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SectionManager : MonoBehaviour
{

    /// <summary>
    /// SectionManager manages the section generation, laying out, stitching, and recycling.
    /// 
    /// Drew Hurdle's Code. Stay out.
    /// 
    /// </summary>

    public class SectionData
    {
        public Section sec;
        public float start;
        public float end;
    }

    SectionData blank;
    
    public class iVec2
    {
        public int x;
        public int y;
        public iVec2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            iVec2 other = obj as iVec2;
            return other != null && other.x == x && other.y == y;
        }
        public override int GetHashCode()
        {
            return x ^ 3 + y ^ 7;
        }
    }

    public int loadRad = 3;

    private iVec2 lastCS;

    public GameObject ramp;

    private int sectionWidth = 100;
    private int sectionHeight = 100;

    Dictionary<iVec2, GameObject> currentSections = new Dictionary<iVec2, GameObject>();

    public GameObject cam;

    public Section[] sections;

    public SectionData[] secData;

    private float maxCommon;

    //These don't really have to be tracked here... I don't think...

    // Use this for initialization
    void Start()
    {

        blank = new SectionData();
        blank.sec = null;
        blank.start = 0;
        blank.end = 0;

        float total = 0;
        foreach (Section s in sections)
        {
            total += s.commonness;
        }
        secData = new SectionData[sections.Length];
        int j = 0;
        float lastEnd = 0;
        float totalSoFar = 0;
        foreach (Section s in sections)
        {
            secData[j] = new SectionData();
            secData[j].sec = s;
            secData[j].start = lastEnd;
            totalSoFar += s.commonness;
            secData[j].end = totalSoFar;
            j++;
        }

        maxCommon = total;

        //On start, load the width and height of sections.
        //Bike is at 0, 0.
        for (int i = -loadRad; i <= loadRad; i++)
        {
            for (int h = -loadRad; h <= loadRad; h++)
            {
                GameObject go = GenSection(new iVec2(h, i));
                if (go != null)
                {
                    currentSections.Add(new iVec2(h, i), go);
                }
            }
        }
    }

    public SectionData findSection(float common)
    {
        for (int i = 0; i < secData.Length; i++)
        {
            if (secData[i].start <= common && secData[i].end > common)
            {
                return secData[i];
            }
        }
        return null;
    }

    public GameObject GenSection(iVec2 cor)
    {
        iVec2 corBehind = new iVec2(cor.x, cor.y - 1);
        Section secBehindMe = null;

        if (currentSections.ContainsKey(corBehind))
        {
            secBehindMe = currentSections[corBehind].GetComponent<Section>();
        }
        Vector3 pos = new Vector3(cor.x * sectionHeight, 0, cor.y * sectionWidth);

        if (secBehindMe == null || secBehindMe.sectionsInFront == null || secBehindMe.sectionsInFront.Count == 0)
        {
            

            float common = UnityEngine.Random.value * maxCommon;
            SectionData sd = findSection(common);
            if (cor.y <= 5)
            {
                return null;
            }
            else
            {
                GameObject go = (GameObject)Instantiate(sd.sec.gameObject, pos, sd.sec.transform.rotation);

                go.GetComponent<Section>().diff = cor.y;

                return go;
            }
        }
        else
        {
            float thisMaxCommon = 0;
            foreach(Section s in secBehindMe.sectionsInFront)
            {
                thisMaxCommon += s.commonness;
            }
            float common = UnityEngine.Random.value * thisMaxCommon;
            float totalCom = 0.0f;
            foreach(Section s in secBehindMe.sectionsInFront)
            {
                if (common >= totalCom && common < totalCom + s.commonness)
                {
                    GameObject go = (GameObject)Instantiate(s.gameObject, pos, s.transform.rotation);
                    go.GetComponent<Section>().diff = cor.y;
                    return go;
                }
                totalCom += s.commonness;
            }
        }


        /* Old mode with cube generator.
        GameObject go = (GameObject)Instantiate(CubeSection, pos, CubeSection.transform.rotation);

        if (Mathf.Abs(cor.y) < 3)
        {
            ((CubeFieldGeneration)(go.GetComponent<CubeFieldGeneration>())).numToSpawn = 0;
        }
        else
        {
        }

        
        if (UnityEngine.Random.value < 0.5)
        {
            ((CubeFieldGeneration)(go.GetComponent<CubeFieldGeneration>())).toSpawn = ramp;
        }

        ((CubeFieldGeneration)(go.GetComponent<CubeFieldGeneration>())).numToSpawn = cor.y < 2? 0 : (cor.y < 60? (int)(0.3f * (cor.y)) : 20);


        return go;
        */
        return null;
    }

    public void RemoveSection(iVec2 cor)
    {
        currentSections.Remove(cor);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSections();
    }

    public void UpdateSections()
    {
        iVec2 cs = GetCamSection();

        Dictionary<iVec2, GameObject> oldSecs = new Dictionary<iVec2, GameObject>();
        foreach (KeyValuePair<iVec2, GameObject> s in currentSections)
        {
            oldSecs.Add(s.Key, s.Value);
        }

        if (!cs.Equals(lastCS))
        {

            
            Dictionary<iVec2, GameObject> newSections = new Dictionary<iVec2, GameObject>();

            for (int x = cs.x -loadRad; x <= cs.x + loadRad; x++)
            {
                for (int y = cs.y ; y <= cs.y + 2 * loadRad; y++)
                {
                    iVec2 t = new iVec2(x, y);
                    if (oldSecs.ContainsKey(t))
                    {
                        GameObject sec = oldSecs[t];
                        newSections.Add(t, sec);
                        oldSecs.Remove(t);
                    }
                    else
                    {
                        //Generate it.
                        GameObject go = GenSection(t);
                        if (go != null)
                        {
                            newSections.Add(t,go);
                        }
                    }
                }
            }
            //Anything left in the current Sections needs to be recycled.
            foreach (KeyValuePair<iVec2, GameObject> k in oldSecs)
            {
                Destroy(k.Value.gameObject);
            }
            currentSections = newSections;

            if (currentSections.ContainsKey(cs))
            {
                foreach (Collider c in currentSections[cs].GetComponentsInChildren<Collider>())
                {
                    if (c.isTrigger)
                    {
                        c.enabled = true;
                    }
                }
            }

            lastCS = cs;
        }

    }

    public iVec2 GetCamSection()
    {
        //Find the section the camera is currently in for opposite stuff.
        Vector3 playerPos = cam.transform.position;
        int secX = (int)((playerPos.x + 0.5f) / sectionWidth);
        int secY = (int)((playerPos.z + 0.5f) / sectionHeight);

        return new iVec2(secX, secY);
    }
}