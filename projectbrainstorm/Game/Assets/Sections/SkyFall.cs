﻿using UnityEngine;
using System.Collections;

public class SkyFall : MonoBehaviour {

    float fallSpeed = 100;
    float startY = -200;
    float origY;
    bool isDone = false;
	// Use this for initialization
	void Start () {
        origY = transform.position.y;
        Vector3 newPos = transform.position;
        newPos.y = startY;
        transform.position = newPos;
	}
	

    public bool IsDone()
    {
        return isDone;
    }
	// Update is called once per frame
	void Update () {
        if (!isDone)
        {
            Vector3 newPos = transform.position;
            newPos.y += fallSpeed * Time.deltaTime;
            transform.position = newPos;

            if (transform.position.y >= origY)
            {
                transform.position = new Vector3(transform.position.x, origY, transform.position.z);
                isDone = true;
            }
        }
	}
}
