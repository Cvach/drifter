﻿using UnityEngine;
using System.Collections;

public class Towers : MonoBehaviour {

	// Use this for initialization
	void Start () {
        float diff = (float)transform.root.GetComponent<Section>().diff;

        float origScale = this.transform.localScale.x;
        float scale = diff / 100.0f;
        if (scale > 1) scale = 1;

        Vector3 s = transform.localScale;
        s.x = scale * origScale;
        transform.localScale = s;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
