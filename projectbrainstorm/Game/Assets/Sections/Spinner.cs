﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

    public float maxSpinSpeed;
    private float spinSpeed;
	// Use this for initialization
	void Start () {
        float diff = (float)transform.root.GetComponent<Section>().diff;

        float scale = (diff + 20) / 100.0f;
        if (scale > 1) scale = 1;

        spinSpeed = maxSpinSpeed * scale;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 rot = transform.localRotation.eulerAngles;
        rot.z += spinSpeed * Time.deltaTime;
        transform.localRotation = Quaternion.Euler(rot);
    }
}
